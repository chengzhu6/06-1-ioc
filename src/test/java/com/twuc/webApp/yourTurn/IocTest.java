package com.twuc.webApp.yourTurn;

import com.twuc.out.OutOfScopeClass;
import com.twuc.webApp.entity.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
class IocTest {

    private AnnotationConfigApplicationContext context;

    @BeforeEach
    private void initTestContext() {
        context = new AnnotationConfigApplicationContext("com.twuc.webApp");
    }

    @Test
    void should_create_object_without_dependency() {
        Contract contract = context.getBean(Contract.class);
        assertEquals(Contract.class, contract.getClass());
    }

    @Test
    void should_not_create_bean_when_out_of_scan_scope() {
        assertThrows(Exception.class, () -> context.getBean(OutOfScopeClass.class));
    }

    @Test
    void shoal_first_execute_constructor() {
        WithAutowiredMethod withAutowiredMethod = context.getBean(WithAutowiredMethod.class);
        Dependent dependent = withAutowiredMethod.getDependent();
        assertNotNull(dependent);
        assertEquals("this is dependent inject", withAutowiredMethod.getMyLogger().getLogger().get(0));
        assertEquals("this is another dependent inject", withAutowiredMethod.getMyLogger().getLogger().get(1));
    }


    @Test
    void should_create_object() {

        WithoutDependency bean = context.getBean(WithoutDependency.class);
        assertNotNull(bean);
    }

    @Test
    void should_create_instance_with_dependent() {

        WithDependency bean = context.getBean(WithDependency.class);
        assertNotNull(bean.getDependent());
        assertNotNull(bean);
    }

    @Test
    void should_get_bean_by_interface() {

        Interface bean = context.getBean(Interface.class);
        assertNotNull(bean);
    }

    @Test
    void should_get_dependent_name_by_interface() {

        SimpleObject bean = (SimpleObject)context.getBean(SimpleInterface.class);
        assertEquals("O_o", bean.getSimpleDependent().getName());
    }

    @Test
    void should_execute_bean_factory_method() {

        SimpleObject bean = (SimpleObject)context.getBean(SimpleInterface.class);
        assertEquals("O_o", bean.getSimpleDependent().getName());
    }

    @Test
    void should_execute_specify_constructor() {
        MultipleConstructor bean = context.getBean(MultipleConstructor.class);
        assertNotNull(bean.getDependent());
        assertNull(bean.getName());
    }


    @Test
    void should_get_the_impls() {
        Map<String, InterfaceWithMultipleImpls> beans = context.getBeansOfType(InterfaceWithMultipleImpls.class);
        Object[] sorted = beans.values().stream().map(
                (item) -> item.getClass().getName()
        ).sorted().toArray();
        assertEquals(3, sorted.length);
        assertEquals("com.twuc.webApp.entity.ImplementationA", sorted[0]);
        assertEquals("com.twuc.webApp.entity.ImplementationB", sorted[1]);
        assertEquals("com.twuc.webApp.entity.ImplementationC", sorted[2]);
    }
}
