package com.twuc.webApp.entity;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
@Component
public class MyLogger {

    private ArrayList<String> logger = new ArrayList<>();

    public ArrayList<String> getLogger() {
        return logger;
    }

    public void setLogger(ArrayList<String> logger) {
        this.logger = logger;
    }
}
