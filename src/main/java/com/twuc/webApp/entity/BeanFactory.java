package com.twuc.webApp.entity;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class BeanFactory {
    @Bean
    public SimpleDependent getSimpleDependent() {
        SimpleDependent simpleDependent = new SimpleDependent();
        simpleDependent.setName("O_o");
        return simpleDependent;
    }
}
